#include <vector>
#include <algorithm>
#include <numeric>
#include <iostream>

int sum_intervals(std::vector<std::pair<int, int>> intervals) {

    // Sort intervals intervals[i].first < intervals[i+1].first
    std::sort(intervals.begin(), intervals.end(),[](std::pair<int,int>& p1,
                                                    std::pair<int,int>& p2)
    {
        return p1.first<p2.first;
    });

    // Initial vector cam contain overlapping intervals
    std::vector<std::pair<int,int>> uniqueInterv;
    
    uniqueInterv.push_back(*intervals.begin());

    for_each(intervals.begin(), intervals.end(), [&uniqueInterv](std::pair<int,int>& p)
    {
        // Partial overlapping
        if (uniqueInterv.back().second > p.first &&
            uniqueInterv.back().second < p.second) uniqueInterv.back().second = p.second;

        // No overlapping
        else if (uniqueInterv.back().second < p.first) uniqueInterv.push_back(p);

        // Full overlapping
        // else if (uniqueInterv.back().second > p.second) do nothing
    });

    // Sum:
    int res = std::accumulate(uniqueInterv.begin(),
                              uniqueInterv.end(),
                              0,
                              [](int& i1, std::pair<int, int>& i2) 
                              {
                                  return i1 + std::abs(i2.second - i2.first);
                              });

    return res;
}

int main()
{
    std::vector<std::pair<int,int>> intervals = { {-141,64}, {235,384}, {-184,324},
                                                  {73,110}, {-85,3}, {369,435},
                                                  {-250,-57}, {-138,-117}, {-193,-27},
                                                  {-176,417}, {425,444}, {497,499},
                                                  {226,355}  }; // 696
    std::cout << sum_intervals(intervals);
}